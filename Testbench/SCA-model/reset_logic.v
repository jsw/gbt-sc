/////////////////////////////////////////////////////////////////////////////
// Design Name:....GBT_SCA                                                 //
// Module Name:....ResetLogic.v (TRI Version)                              //
// Company:........CERN (PH-ESE)                                           //
// Made by:........Alessandro Caratelli                                    //
//                                                                         //
// Date:...........17/Aug/2013 - Created                                   //
//                 13/Dec/2013 - Changed adding voters                     //
//                 31/Jan/2014 - changed voter & resyntetised in 3 steps   //
/////////////////////////////////////////////////////////////////////////////

// -> Define "use_syn_netlist" and set the "cadence dc::set_dont_touch [..]"
//    pragma on the instantiation of this module to avoid unwanted optimizations

`define use_syn_netlist
`timescale 1ns / 1ps


`ifndef use_syn_netlist /////////////////////////////////////////////////////

module ResetLogicVoted(
	input  clk_1,          clk_2,          clk_3,
	input  PromtReset_1,   PromtReset_2,	
	input  ElinkReset_1,   ElinkReset_2,   ElinkReset_3,
 	input  PowerUpReset_1, PowerUpReset_2, PowerUpReset_3,	
	output wire SystemReset_1,  SystemReset_2,  SystemReset_3
	);
	wire reset_1_unvoted, reset_2_unvoted, reset_3_unvoted;
	
	// cadence dc::set_dont_touch RST_Logic
	
	ResetLogic RST_Logic(
		.ElinkReset_1(ElinkReset_1),
		.ElinkReset_2(ElinkReset_2),
		.ElinkReset_3(ElinkReset_3),
 		.PowerUpReset_1(PowerUpReset_1),
		.PowerUpReset_2(PowerUpReset_2),
		.PowerUpReset_3(PowerUpReset_3),
		.PromtReset_1(PromtReset_1),
		.PromtReset_2(PromtReset_2),	
		.SystemReset_1(reset_1_unvoted),
		.SystemReset_2(reset_2_unvoted),
		.SystemReset_3(reset_3_unvoted),
		.clk_1(~clk_1),
		.clk_2(~clk_2),
		.clk_3(~clk_3)
	); 
	res_voter mvres1 (.in1(reset_1_unvoted),.in2(reset_2_unvoted),.in3(reset_3_unvoted), .out(SystemReset_1));
	res_voter mvres2 (.in1(reset_2_unvoted),.in2(reset_3_unvoted),.in3(reset_1_unvoted), .out(SystemReset_2));     
	res_voter mvres3 (.in1(reset_3_unvoted),.in2(reset_1_unvoted),.in3(reset_2_unvoted), .out(SystemReset_3));
	
endmodule

module res_voter(
	input  in1, in2, in3,
	output wire out
	);
	assign out = (in1 != in2) ? in3 : in1;
endmodule 


module ResetLogic (
	input  clk_1,          clk_2,          clk_3,
	input  PromtReset_1,   PromtReset_2,	//
	input  ElinkReset_1,   ElinkReset_2,   ElinkReset_3,//
 	input  PowerUpReset_1, PowerUpReset_2, PowerUpReset_3,//	
	output SystemReset_1,  SystemReset_2,  SystemReset_3//
	);
	
	// cadence dc::set_dont_touch RstOr1
	// cadence dc::set_dont_touch RstOr2
	// cadence dc::set_dont_touch RstOr3
	// cadence dc::set_dont_touch promtAnd1
	// cadence dc::set_dont_touch promtAnd2
	// cadence dc::set_dont_touch promtAnd3
	
	ResMonostable ResMonostable_1 (.in(ElinkReset_1), .out(ElinkResEvent_1), .clk(clk_1), .res(PowerUpReset_1));
	ResMonostable ResMonostable_2 (.in(ElinkReset_2), .out(ElinkResEvent_2), .clk(clk_2), .res(PowerUpReset_2));
	ResMonostable ResMonostable_3 (.in(ElinkReset_3), .out(ElinkResEvent_3), .clk(clk_3), .res(PowerUpReset_3));
	
	AND2_B promtAnd1(.A(PromtReset_1), .B(PromtReset_2), .Z (promtOut1));
	AND2_B promtAnd2(.A(PromtReset_1), .B(PromtReset_2), .Z (promtOut2));
	AND2_B promtAnd3(.A(PromtReset_1), .B(PromtReset_2), .Z (promtOut3));	
	
	OR3_B RstOr1 (.Z(SystemReset_1), .A(ElinkResEvent_1), .B(promtOut1), .C(PowerUpReset_1));
	OR3_B RstOr2 (.Z(SystemReset_2), .A(ElinkResEvent_2), .B(promtOut2), .C(PowerUpReset_2));
	OR3_B RstOr3 (.Z(SystemReset_3), .A(ElinkResEvent_3), .B(promtOut3), .C(PowerUpReset_3));

endmodule

`timescale 1ns / 1ps
module ResMonostable (
	input  clk,
	input  res,
	input  in,
	output out
);	
	Dflipflop catch(.D(1'b1), .Q(ev),  .clk(in),  .RN(~reset_del));
	Dflipflop store(.D(ev),   .Q(out), .clk(clk), .RN(~res));
	
	DELAY6_C delay_reset (.Z(reset_del1),.A(out)
		`ifndef RC
			, .VDD(), .GND(), .NW(), .SX()
		`endif
	);
	DELAY6_C delay_reset1 (.Z(reset_del2),.A(reset_del1)
		`ifndef RC
			, .VDD(), .GND(), .NW(), .SX()
		`endif
	);
	DELAY6_C delay_reset2 (.Z(reset_del3),.A(reset_del2)
		`ifndef RC
			, .VDD(), .GND(), .NW(), .SX()
		`endif
	);
	DELAY6_C delay_reset3 (.Z(reset_del4),.A(reset_del3)
		`ifndef RC
			, .VDD(), .GND(), .NW(), .SX()
		`endif
	);
	DELAY6_C delay_reset4 (.Z(reset_del5),.A(reset_del4)
		`ifndef RC
			, .VDD(), .GND(), .NW(), .SX()
		`endif
	);
	DELAY6_C delay_reset5 (.Z(reset_del),.A(reset_del5)
		`ifndef RC
			, .VDD(), .GND(), .NW(), .SX()
		`endif
	);		
endmodule	
	
`timescale 1ns / 1ps	
module Dflipflop(
	input  clk, RN, D,
	output reg Q
);
	always @(posedge clk or negedge RN) begin
		if (~RN) Q <=#1 1'b0;
		else Q <=#1 D;
	end
endmodule	



`else ////////////////////////////////////////////////////////////////////

// Generated by Cadence Encounter(R) RTL Compiler RC9.1.203 - v09.10-s242_1

module Dflipflop_r2g_r2g(clk, RN, D, Q);
  input clk, RN, D;
  output Q;
  wire clk, RN, D;
  wire Q;
  DFFR_E Q_reg(.RN (RN), .CLK (clk), .D (1'b1), .Q (Q), .QBAR ());
endmodule

module Dflipflop_r2g_5_r2g(clk, RN, D, Q);
  input clk, RN, D;
  output Q;
  wire clk, RN, D;
  wire Q;
  DFFR_E Q_reg(.RN (RN), .CLK (clk), .D (D), .Q (Q), .QBAR ());
endmodule

module ResMonostable_r2g_r2g(clk, res, in, out);
  input clk, res, in;
  output out;
  wire clk, res, in;
  wire out;
  wire ev, n_0, n_1, reset_del, reset_del1, reset_del2, reset_del3,
       reset_del4;
  wire reset_del5;
  Dflipflop_r2g_r2g catch(.clk (in), .RN (n_1), .D (1'b1), .Q (ev));
  Dflipflop_r2g_5_r2g store(.clk (clk), .RN (n_0), .D (ev), .Q (out));
  DELAY6_C delay_reset(.A (out), .Z (reset_del1));
  DELAY6_C delay_reset1(.A (reset_del1), .Z (reset_del2));
  DELAY6_C delay_reset2(.A (reset_del2), .Z (reset_del3));
  DELAY6_C delay_reset3(.A (reset_del3), .Z (reset_del4));
  DELAY6_C delay_reset4(.A (reset_del4), .Z (reset_del5));
  DELAY6_C delay_reset5(.A (reset_del5), .Z (reset_del));
  INVERT_A p214748365A(.A (reset_del), .Z (n_1));
  INVERT_A p214748365A3(.A (res), .Z (n_0));
endmodule

module Dflipflop_r2g_4_r2g(clk, RN, D, Q);
  input clk, RN, D;
  output Q;
  wire clk, RN, D;
  wire Q;
  DFFR_E Q_reg(.RN (RN), .CLK (clk), .D (1'b1), .Q (Q), .QBAR ());
endmodule

module Dflipflop_r2g_3_r2g(clk, RN, D, Q);
  input clk, RN, D;
  output Q;
  wire clk, RN, D;
  wire Q;
  DFFR_E Q_reg(.RN (RN), .CLK (clk), .D (D), .Q (Q), .QBAR ());
endmodule

module ResMonostable_r2g_12_r2g(clk, res, in, out);
  input clk, res, in;
  output out;
  wire clk, res, in;
  wire out;
  wire ev, n_0, n_1, reset_del, reset_del1, reset_del2, reset_del3,
       reset_del4;
  wire reset_del5;
  Dflipflop_r2g_4_r2g catch(.clk (in), .RN (n_1), .D (1'b1), .Q (ev));
  Dflipflop_r2g_3_r2g store(.clk (clk), .RN (n_0), .D (ev), .Q (out));
  DELAY6_C delay_reset(.A (out), .Z (reset_del1));
  DELAY6_C delay_reset1(.A (reset_del1), .Z (reset_del2));
  DELAY6_C delay_reset2(.A (reset_del2), .Z (reset_del3));
  DELAY6_C delay_reset3(.A (reset_del3), .Z (reset_del4));
  DELAY6_C delay_reset4(.A (reset_del4), .Z (reset_del5));
  DELAY6_C delay_reset5(.A (reset_del5), .Z (reset_del));
  INVERT_A p214748365A(.A (reset_del), .Z (n_1));
  INVERT_A p214748365A3(.A (res), .Z (n_0));
endmodule

module Dflipflop_r2g_2_r2g(clk, RN, D, Q);
  input clk, RN, D;
  output Q;
  wire clk, RN, D;
  wire Q;
  DFFR_E Q_reg(.RN (RN), .CLK (clk), .D (1'b1), .Q (Q), .QBAR ());
endmodule

module Dflipflop_r2g_1_r2g(clk, RN, D, Q);
  input clk, RN, D;
  output Q;
  wire clk, RN, D;
  wire Q;
  DFFR_E Q_reg(.RN (RN), .CLK (clk), .D (D), .Q (Q), .QBAR ());
endmodule

module ResMonostable_r2g_11_r2g(clk, res, in, out);
  input clk, res, in;
  output out;
  wire clk, res, in;
  wire out;
  wire ev, n_0, n_1, reset_del, reset_del1, reset_del2, reset_del3,
       reset_del4;
  wire reset_del5;
  Dflipflop_r2g_2_r2g catch(.clk (in), .RN (n_1), .D (1'b1), .Q (ev));
  Dflipflop_r2g_1_r2g store(.clk (clk), .RN (n_0), .D (ev), .Q (out));
  DELAY6_C delay_reset(.A (out), .Z (reset_del1));
  DELAY6_C delay_reset1(.A (reset_del1), .Z (reset_del2));
  DELAY6_C delay_reset2(.A (reset_del2), .Z (reset_del3));
  DELAY6_C delay_reset3(.A (reset_del3), .Z (reset_del4));
  DELAY6_C delay_reset4(.A (reset_del4), .Z (reset_del5));
  DELAY6_C delay_reset5(.A (reset_del5), .Z (reset_del));
  INVERT_A p214748365A(.A (reset_del), .Z (n_1));
  INVERT_A p214748365A3(.A (res), .Z (n_0));
endmodule

module ResetLogic_r2g(clk_1, clk_2, clk_3, PromtReset_1, PromtReset_2,
     ElinkReset_1, ElinkReset_2, ElinkReset_3, PowerUpReset_1,
     PowerUpReset_2, PowerUpReset_3, SystemReset_1, SystemReset_2,
     SystemReset_3);
  input clk_1, clk_2, clk_3, PromtReset_1, PromtReset_2, ElinkReset_1,
       ElinkReset_2, ElinkReset_3, PowerUpReset_1, PowerUpReset_2,
       PowerUpReset_3;
  output SystemReset_1, SystemReset_2, SystemReset_3;
  wire clk_1, clk_2, clk_3, PromtReset_1, PromtReset_2, ElinkReset_1,
       ElinkReset_2, ElinkReset_3, PowerUpReset_1, PowerUpReset_2,
       PowerUpReset_3;
  wire SystemReset_1, SystemReset_2, SystemReset_3;
  wire ElinkResEvent_1, ElinkResEvent_2, ElinkResEvent_3, n_3, n_4,
       n_19, n_21, n_23;
  wire promtOut1, promtOut2, promtOut3;
  ResMonostable_r2g_r2g ResMonostable_1(.clk (clk_1), .res
       (PowerUpReset_1), .in (ElinkReset_1), .out (ElinkResEvent_1));
  ResMonostable_r2g_12_r2g ResMonostable_2(.clk (clk_2), .res
       (PowerUpReset_2), .in (ElinkReset_2), .out (ElinkResEvent_2));
  ResMonostable_r2g_11_r2g ResMonostable_3(.clk (clk_3), .res
       (PowerUpReset_3), .in (ElinkReset_3), .out (ElinkResEvent_3));
  OR3_B RstOr1(.A (ElinkResEvent_1), .B (promtOut1), .C
       (PowerUpReset_1), .Z (n_21));
  OR3_B RstOr2(.A (ElinkResEvent_2), .B (promtOut2), .C
       (PowerUpReset_2), .Z (n_23));
  OR3_B RstOr3(.A (ElinkResEvent_3), .B (promtOut3), .C
       (PowerUpReset_3), .Z (n_19));
  BUFFER_F drc(.A (n_19), .Z (SystemReset_3));
  BUFFER_F drc8(.A (n_21), .Z (SystemReset_1));
  BUFFER_F drc9(.A (n_23), .Z (SystemReset_2));
  BUFFER_C p214748365A5(.A (PromtReset_1), .Z (n_3));
  BUFFER_C p214748365A7(.A (PromtReset_2), .Z (n_4));
  AND2_B promtAnd1(.A (n_3), .B (n_4), .Z (promtOut1));
  AND2_B promtAnd2(.A (n_3), .B (n_4), .Z (promtOut2));
  AND2_B promtAnd3(.A (n_3), .B (n_4), .Z (promtOut3));
endmodule

module res_voter_r2g(in1, in2, in3, out);
  input in1, in2, in3;
  output out;
  wire in1, in2, in3;
  wire out;
  ADDF_F g2(.A (in3), .B (in1), .CIN (in2), .COUT (out), .SUM ());
endmodule

module res_voter_r2g_2(in1, in2, in3, out);
  input in1, in2, in3;
  output out;
  wire in1, in2, in3;
  wire out;
  ADDF_F g2(.A (in3), .B (in1), .CIN (in2), .COUT (out), .SUM ());
endmodule

module res_voter_r2g_1(in1, in2, in3, out);
  input in1, in2, in3;
  output out;
  wire in1, in2, in3;
  wire out;
  ADDF_F g2(.A (in3), .B (in1), .CIN (in2), .COUT (out), .SUM ());
endmodule

module ResetLogicVoted(clk_1, clk_2, clk_3, PromtReset_1,
     PromtReset_2, ElinkReset_1, ElinkReset_2, ElinkReset_3,
     PowerUpReset_1, PowerUpReset_2, PowerUpReset_3, SystemReset_1,
     SystemReset_2, SystemReset_3);
  input clk_1, clk_2, clk_3, PromtReset_1, PromtReset_2, ElinkReset_1,
       ElinkReset_2, ElinkReset_3, PowerUpReset_1, PowerUpReset_2,
       PowerUpReset_3;
  output SystemReset_1, SystemReset_2, SystemReset_3;
  wire clk_1, clk_2, clk_3, PromtReset_1, PromtReset_2, ElinkReset_1,
       ElinkReset_2, ElinkReset_3, PowerUpReset_1, PowerUpReset_2,
       PowerUpReset_3;
  wire SystemReset_1, SystemReset_2, SystemReset_3;
  wire n_0, n_1, n_2, n_4, n_5, n_6, reset_1_unvoted, reset_2_unvoted;
  wire reset_3_unvoted;
  ResetLogic_r2g RST_Logic(.clk_1 (n_4), .clk_2 (n_2), .clk_3 (n_1),
       .PromtReset_1 (PromtReset_1), .PromtReset_2 (PromtReset_2),
       .ElinkReset_1 (ElinkReset_1), .ElinkReset_2 (ElinkReset_2),
       .ElinkReset_3 (ElinkReset_3), .PowerUpReset_1 (n_6),
       .PowerUpReset_2 (n_0), .PowerUpReset_3 (n_5), .SystemReset_1
       (reset_1_unvoted), .SystemReset_2 (reset_2_unvoted),
       .SystemReset_3 (reset_3_unvoted));
  res_voter_r2g mvres1(.in1 (reset_1_unvoted), .in2 (reset_2_unvoted),
       .in3 (reset_3_unvoted), .out (SystemReset_1));
  res_voter_r2g_2 mvres2(.in1 (reset_2_unvoted), .in2
       (reset_3_unvoted), .in3 (reset_1_unvoted), .out (SystemReset_2));
  res_voter_r2g_1 mvres3(.in1 (reset_3_unvoted), .in2
       (reset_1_unvoted), .in3 (reset_2_unvoted), .out (SystemReset_3));
  BUFFER_C p214748365A9(.A (PowerUpReset_1), .Z (n_6));
  BUFFER_C p214748365A10(.A (PowerUpReset_3), .Z (n_5));
  INVERT_A p214748365A11(.A (clk_1), .Z (n_4));
  INVERT_A p214748365A13(.A (clk_2), .Z (n_2));
  INVERT_A p214748365A14(.A (clk_3), .Z (n_1));
  BUFFER_C p214748365A15(.A (PowerUpReset_2), .Z (n_0));
endmodule

`endif
