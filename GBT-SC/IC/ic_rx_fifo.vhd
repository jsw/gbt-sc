-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @date April, 2017
--! @version 1.0
--! @brief IC control - RX FIFO
-------------------------------------------------------

--! Include the IEEE VHDL standard library
library ieee;
--! Use STD_Logic to define vector types
use ieee.std_logic_1164.all;
--! Used to convert std_logic_vector to integer and manage fifo pointer
use ieee.numeric_std.all;

--! @brief IC_rx_fifo Entity - RX FIFO
--! @details
--! The IC_rx_fifo is used to store the reply from the GBTx
--! following a read/write command. It extracts the header 
--! details (GBTx address, number of words, etc.) and provide
--! a generic fifo interface to get the data.
entity ic_rx_fifo is
    generic (
        g_FIFO_DEPTH    : integer := 10;                                    --! Depth of the internal FIFO used to improve the timming performance
        g_WORD_SIZE     : integer := 8                                      --! Size of the words to be stored
    );
    port (
        clk_i           : in  std_logic;                                    --! Rx clock (Rx_frameclk_o from GBT-FPGA IP): must be a multiple of the LHC frequency
        rx_clk_en       : in  std_logic;                                    --! Rx clock enable signal must be used in case of multi-cycle path(rx_clk_i > LHC frequency). By default: always enabled
		  
        reset_i         : in  std_logic;                                    --! Reset all of the RX processes

        -- Data
        data_i          : in  std_logic_vector((g_WORD_SIZE-1) downto 0);   --! Input data (from deserializer)
        data_o          : out std_logic_vector((g_WORD_SIZE-1) downto 0);   --! Data from the FIFO (read by the user)

        -- Control
        new_word_i      : in  std_logic;                                    --! New command received, end of frame (from deserializer)
        write_i         : in  std_logic;                                    --! Request a write into the FIFO (from deserializer)
        read_i          : in  std_logic;                                    --! Request a read (from user)
        
        --Status
        gbtx_addr_o     : out std_logic_vector(7 downto 0);                 --! Info from the frame header (GBTx I2C address)
        mem_ptr_o       : out std_logic_vector(15 downto 0);                --! Info from the frame header (address of the first register)
        nb_of_words_o   : out std_logic_vector(15 downto 0);                --! Number of words/bytes received
        
        to_be_read_o    : out std_logic                                     --! Ready to be read, FIFO is not empty
    );
end ic_rx_fifo;

--! @brief IC_rx_fifo Architecture - RX FIFO
--! @details 
--! The IC_rx_fifo architecture implements the logic to store the received byte
--! and extract the header when a new_word_i flag is received.
architecture behavioral of ic_rx_fifo is

    -- Types
    subtype reg_t   is std_logic_vector((g_WORD_SIZE-1) downto 0);
    type ramreg_t   is array(integer range<>) of reg_t;

    -- Signals
    signal mem_arr              : ramreg_t(g_FIFO_DEPTH+7 downto 0);

    signal wr_ptr               : integer range 0 to g_FIFO_DEPTH+7;
    signal word_in_mem_size     : integer range 0 to g_FIFO_DEPTH+7;
    signal rd_ptr               : integer range 0 to g_FIFO_DEPTH+7;

    signal set_out              : std_logic := '0';    

begin                 --========####   Architecture Body   ####========-- 

    ram_proc: process(reset_i, clk_i)

    begin

        if reset_i = '1' then
            wr_ptr              <= 0;
            rd_ptr              <= 0;

            data_o              <= (others => '0');

            word_in_mem_size    <= 0;

        elsif rising_edge(clk_i) then
		  
            -- Read
            if read_i = '1' and rd_ptr < word_in_mem_size then
                data_o          <= mem_arr(rd_ptr);
                rd_ptr          <= rd_ptr + 1;
            end if;

            if word_in_mem_size > rd_ptr then
                to_be_read_o    <= '1';
            else
                to_be_read_o    <= '0';
            end if;
			
            if rx_clk_en = '1' then
            
                -- Write
                if write_i = '1' then
                    mem_arr(wr_ptr) <= data_i;
                    wr_ptr <= wr_ptr + 1;
                end if;
            
                if new_word_i = '1' then
                 
                    if wr_ptr > 7 then
                        gbtx_addr_o             <= '0' & mem_arr(1)(7 downto 1);
                        nb_of_words_o           <= mem_arr(4) & mem_arr(3);
                        mem_ptr_o               <= mem_arr(6) & mem_arr(5);
                        word_in_mem_size        <= wr_ptr-1;
                        rd_ptr                  <= 7;

                    else
                        word_in_mem_size        <= 0;
                        rd_ptr                  <= 0;

                    end if;

                    wr_ptr <= 0;

                end if;
           
			end if;
			
        end if;

    end process;

end behavioral;
--============================================================================--
--############################################################################--
--============================================================================--
